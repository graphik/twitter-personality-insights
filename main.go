package main

import (
	"gitlab.com/graphik/twitter-personality-insights/cmd"
)

func main() {
	cmd.Execute()
}
