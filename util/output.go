package util

import (
	"encoding/json"
	"os"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"

	"gitlab.com/graphik/twitter-personality-insights/pi"
)

// TableCreator - create a tablewriter with headers.
func TableCreator(headers []string) *tablewriter.Table {

	formattedTable := tablewriter.NewWriter(os.Stdout)
	formattedTable.SetHeader(headers)

	return formattedTable
}

// ExportToJSON - export a raw response to a JSON file
func ExportToJSON(p pi.APIResponse, u string) {

	// Set indentation for pretty printing.
	indent, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		log.Error("Could not indent json:", err)
	}

	out := append(indent, '\n')

	f, err := os.Create(u + ".json")
	if err != nil {
		log.Println("Could not create file: ", err)
	}

	defer f.Close()

	_, err = f.Write(out)
	if err != nil {
		log.Error("Could not write to file:", err)
	}
}
