### Twitter Personality Insights

A simple, command line `golang` implementation of the IBM Personality Insights [demo](https://personality-insights-demo.ng.bluemix.net/).

This tool extends the demo to include [Tone Analysis](https://www.ibm.com/watson/services/tone-analyzer/).

Output will return the full suite of results, including the Big 5 traits, needs, values, behavior distribution and consumption preferences.

#### Requirements.

To query Twitter, set up a [development account](https://dev.twitter.com), create an application, and generate the full set of API and access keys. 

To query the Personality Insights service, create an [IBM Cloud](https://www.ibm.com/cloud/) account and create a new Personality Insights service. Once the service has been deployed, the relevant credentials will be available in the IBM Cloud service dashboard. 

The full set of required credentials:

1. Twitter Consumer API key
2. Twitter Consumer API secret key
3. Twitter Access Token
4. Twitter Access Token Secret
5. IBM Watson Services Personality Insights API Key
7. IBM Watson Services Personality Insights URL

#### Quickstart.

Grab the correct platform binary from the [releases](https://gitlab.com/graphik/twitter-personality-insights/tags).

OR

Clone the repository and build the source for your platform:

```
$ make
```

After compilation, the resulting `tpi` binary will be available in the project's root directory.

Create a configuration file called `config.yaml` (or rename the example), and enter the credential information fetched in the **Requirements** step above.

See the command help for basic usage instructions.

```
$ ./tpi

Twitter Personality Insights

Usage:
  tpi [flags]
  tpi [command]

Available Commands:
  help        Help about any command
  query       query the personality insights service

Flags:
  -h, --help      help for tpi
      --version   Display version information
```
##### Example:

Send a query, include retweets in the search (`-r/--include-retweets`), and return only the major category results (`-c/--concise`):

```
$ ./tpi query -u twitterUsername -c -r

Fetching tweets for user: @twitterUsername
Found 349 tweets for user: @twitterUsername
Creating CI payload and sending request to: https://gateway.watsonplatform.net/personality-insights/api

Personality Insights
--------------------

Username: @twitterUsername
Total words processed: 3771

Trait: Openness
Percentile: 94.78%
Raw Score: 0.80051

Trait: Conscientiousness
Percentile: 54.41%
Raw Score: 0.54854

Trait: Extraversion
Percentile: 36.13%
Raw Score: 0.36517

Trait: Agreeableness
Percentile: 23.25%
Raw Score: 0.66886

Trait: Emotional range
Percentile: 22.58%
Raw Score: 0.42494
```

