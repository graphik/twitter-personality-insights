package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/graphik/twitter-personality-insights/config"
)

var (
	version bool

	username        string
	concise         bool
	export          bool
	includeRetweets bool
	writeInput      bool

	userConfig Configuration

	// RootCmd - The main CLI command.
	RootCmd = &cobra.Command{
		Use:   "tpi",
		Short: "Twitter Personality Insights",
		Run: func(cmd *cobra.Command, args []string) {
			if version {
				fmt.Printf("%s\n", config.Version)
				os.Exit(0)
			} else {
				cmd.Help()
			}
		},
	}
)

func init() {

	cobra.OnInitialize(initConfig)

	RootCmd.Flags().BoolVar(&version, "version", false, "Display version information")

	queryCmd.Flags().StringVarP(&username, "username", "u", "", "twitter username to query")
	queryCmd.Flags().BoolVarP(&export, "export", "e", false, "export the raw PI results to a json file")
	queryCmd.Flags().BoolVarP(&concise, "concise", "c", false, "only return the main PI category results")
	queryCmd.Flags().BoolVarP(&includeRetweets, "include-retweets", "r", false, "include retweets in the twitter results")
	queryCmd.Flags().BoolVarP(&writeInput, "write-input", "i", false, "write the input json to disk")

	RootCmd.AddCommand(queryCmd)

	// add an empty line to stdout
	fmt.Println("")

}

// Execute - execute the cli
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

// read a configuration file.
func initConfig() {

	if _, err := os.Stat(config.ConfigFileName); os.IsNotExist(err) {
		if len(os.Args) <= 1 {
			fmt.Println("")
		} else {
			if os.Args[1] != "--version" {
				log.Fatal("Configuration file does not exist: ", config.ConfigFileName)
			}
		}
	} else {
		viper.SetConfigType("yaml")
		viper.SetConfigFile(
			config.ConfigFileName,
		)
		viper.ReadInConfig()

		viper.Unmarshal(&userConfig)

	}

}
