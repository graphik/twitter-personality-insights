package cmd

// Configuration - the user provided configuration
// read in from a file.
type Configuration struct {
	TwitterAPIKey            string `yaml:"twitterapikey"`
	TwitterSecretKey         string `yaml:"twittersecretkey"`
	TwitterAccessToken       string `yaml:"twitteraccesstoken"`
	TwitterAccessTokenSecret string `yaml:"twitteraccesstokensecret"`
	IbmAPIKey                string `yaml:"ibmapikey"`
	IbmURL                   string `yaml:"ibmurl"`
}
