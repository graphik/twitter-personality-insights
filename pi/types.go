package pi

// ContentItems - slice of personality insight content items
type ContentItems struct {
	ContentItems []ContentItem `json:"contentItems"`
}

// ContentItem - a personality insight content item
type ContentItem struct {
	UserID      int64  `json:"userid"`
	ID          int64  `json:"id"`
	SourceID    string `json:"sourceid"`
	ContentType string `json:"contenttype"`
	Language    string `json:"language"`
	Content     string `json:"content"`
	Created     int64  `json:"created"`
	// Reply       int64  `json:"reply,omitempty"`
	Forward bool `json:"forward"`
}

// APIResponse - representation of a successful response from the
// PI API
type APIResponse struct {
	WordCount              int64                   `json:"word_count"`
	ProcessedLanguage      string                  `json:"processed_language"`
	Personality            []Trait                 `json:"personality"`
	Warnings               []Warning               `json:"warnings"`
	Needs                  []Trait                 `json:"needs"`
	Values                 []Trait                 `json:"values"`
	Behavior               []Behavior              `json:"behavior"`
	ConsumptionPreferences []ConsumptionPreference `json:"consumption_preferences"`
}

// Trait - representation of a personality trait
type Trait struct {
	TraitID     string  `json:"trait_id"`
	Name        string  `json:"name"`
	Category    string  `json:"category"`
	Percentile  float64 `json:"percentile,omitempty"`
	RawScore    float64 `json:"raw_score,omitempty"`
	Percentage  float64 `json:"percentage,omitempty"`
	Significant bool    `json:"significant,omitempty"`
	Children    []Trait `json:"children,omitempty"`
}

// ConsumptionPreferences - consumption preference responses
type ConsumptionPreferences struct {
	ConsumptionPreferences []ConsumptionPreference `json:"consumption_preferences"`
}

// ConsumptionPreference - a PI consumption preference
type ConsumptionPreference struct {
	CpCategoryID string       `json:"consumption_preference_category_id"`
	Name         string       `json:"name"`
	Preferences  []Preference `json:"consumption_preferences"`
}

// Preference - a PI consumption preference
type Preference struct {
	PreferenceID string  `json:"consumption_preference_id"`
	Name         string  `json:"name"`
	Score        float64 `json:"score"`
}

// Warning - representation of a warning included in the PI API response
type Warning struct {
	WarningID string `json:"warning_id"`
	Message   string `json:"message"`
}

// Behavior - a behavior
type Behavior struct {
	TraitID    string  `json:"trait_id"`
	Name       string  `json:"name"`
	Category   string  `json:"category"`
	Percentage float64 `json:"percentage"`
}
