package pi

import (
	"encoding/json"

	"github.com/dghubble/go-twitter/twitter"
	log "github.com/sirupsen/logrus"
)

// TweetToContentItem - convert a tweet to a personality insight content item
func TweetToContentItem(item twitter.Tweet) ContentItem {

	createdTime, _ := item.CreatedAtTime()

	ci := ContentItem{
		UserID:      item.User.ID,
		ID:          item.ID,
		SourceID:    "twitter-personality-insights",
		ContentType: "text/plain",
		Language:    item.Lang,
		Content:     item.Text,
		Created:     createdTime.Unix() * 1000,
		Forward:     false,
	}

	return ci

}

// CreateCIPayload - create the formatted JSON payload to send
// to the Personality Insights service.
func CreateCIPayload(tweets []twitter.Tweet) []byte {

	var contentItems ContentItems

	for t := range tweets {
		contentItem := TweetToContentItem(
			tweets[t],
		)
		contentItems.ContentItems = append(contentItems.ContentItems, contentItem)
	}

	jsonCIPayload, err := json.Marshal(contentItems)
	if err != nil {
		log.Error("Could not create json: ", err)
		return jsonCIPayload
	}

	return jsonCIPayload

}
