GITLAB_PROJECT_ID = 13416292
GITLAB_PROJECT_PATH = https://gitlab.com/graphik/twitter-personality-insights
LDFLAGS := -s -w
GITLAB_PROJECT_URL := https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)
PRIVATE_TOKEN := ${GITLAB_TOKEN}

all: build

build:
	go build -o tpi *.go

build-darwin:
	GOOS=darwin GOARCH=amd64 go build -o tpi-darwin -ldflags='$(LDFLAGS)' *.go

build-linux:
	GOOS=linux GOARCH=amd64 go build -o tpi-linux -ldflags='$(LDFLAGS)' *.go

release: build-darwin build-linux release-version release-post release-tag

release-version: 
	$(eval VERSION = $(shell go run main.go --version))
	$(eval VERSION := $(strip $(VERSION)))
	mv tpi-linux tpi-linux-$(VERSION)
	mv tpi-darwin tpi-darwin-$(VERSION)

release-post:
	$(eval LINUX_URL = $(shell curl --request POST --header "PRIVATE-TOKEN: $(PRIVATE_TOKEN)" --form "file=@./tpi-linux-$(VERSION)" $(GITLAB_PROJECT_URL)/uploads | jq -r .url))
	$(eval DARWIN_URL = $(shell curl --request POST --header "PRIVATE-TOKEN: $(PRIVATE_TOKEN)" --form "file=@./tpi-darwin-$(VERSION)" $(GITLAB_PROJECT_URL)/uploads | jq -r .url))
	rm tpi-darwin-* tpi-linux-*

release-tag:
	git tag $(VERSION)
	git push origin --tags
	curl --request POST -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $(PRIVATE_TOKEN)" -d'{"name": "$(VERSION)", "tag_name": "$(VERSION)", "description": "Binaries: [darwin]($(GITLAB_PROJECT_PATH)$(DARWIN_URL)) / [linux]($(GITLAB_PROJECT_PATH)$(LINUX_URL))"}' https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/releases



